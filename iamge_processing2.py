
from PIL import Image 
from termcolor import colored 
import os , sys , shutil 
from pathlib import Path 
import time 

def PathValidation(path_image):
	""" Check folder path """ 
	
	condition_path = False 
	
	while (condition_path == False):
		if not Path(path_image).is_dir() :
			condition_path = False 
			print(colored('Incorrect Path','red'))
			path_image = input("Enter Your Path Images :") 
		else:
			condition_path = True
			print(colored('Correct Path','green'))
	
	if condition_path == False:
		return None
		
	print("-"*60)
	return (path_image)

def CheckThePath(path_images):
	""" Check the path and Return main Path """
		
	if not os.access(path_images, os.F_OK):
		return "Invalid Path"
	return path_images  
	
		
	
def SortingImagesFromDatat(path_images):
	""" Copy Photos Another Folder And Sortin Images """

	try :
		list_images = list(filter(lambda x:x.endswith((".png",".jpg",".jpeg")),os.listdir(path_images)))
		
		return list_images
		
	except Exception as error :
		return error 
	
	except FileExistsError as error :
		return error 


def AddPhotosFolder(images):
	""" Add photos to a new folder """ 
	print("Please enter a valid 'Path' Example :" , colored("/storage/emulated/0/Download","green")) ; print("\n")
	
	
	parent_dir = input("Choose where to create the folder :")
	directory = input("Please write a folder name :") ; print("\n")
	
	
	while not os.access(parent_dir, os.F_OK):
		print(colored("Invalid Path Please Try Again","red"))
		
		parent_dir = input("Choose where to create the folder :")
		directory = input("Please write a folder name :")
		
	path = os.path.join(parent_dir, directory)
	print("-"*60) ; return path 
	
		

def CopyPhotosAnotherFolder(new_path , list_images , old_path):
	""" Copy photos to another folder """ 
	condition_path = False 
	
	if os.path.exists(new_path):
		return (colored("The track you requested is already available\n","yellow"))
	
	elif not Path(new_path).is_dir() == False:
		condition_path = False 
	else:
		condition_path = True 
	
	if condition_path == False:
		return(colored("Invalid Path Please Try Again","red"))
	
	else :
		os.mkdir(new_path) # Add Images to New Path 
		print(colored("The path is correct. The operation completed successfully\n" , "green")) ; print("-"*60)
		
		
	for image in list_images :
		shutil.copy(old_path + "/" + image , new_path)
	return new_path 


def ShowLargerImage(image_path):
	""" View larger image """
	dict_pixels = dict()
	
	try :
		for (root , isdir , file) in os.walk(image_path):
			for target_image in file :
				
				image = Image.open(root + "/" + target_image)
				width , height = image.size 
				
				dict_pixels[(target_image)] = sum([width , height])
		
		bigger_picture , name__image = 0,"" 
		for (name_image , pixels) in dict_pixels.items():
			
			if (pixels > bigger_picture):
				name__image = name_image 
				bigger_picture = pixels 
		
		print("-"*60)
		print(colored("Example :storage/emulated/0/Alaa ","yellow"))
		
		choice_save_image = input("Choose a location to save the image :")

		while not os.access(choice_save_image, os.F_OK):
			print(colored("Error Please Try Again Please Enter a valid path\n!","red"))
			choice_save_image = input("Choose a location to save the image\n :")
		
		if Path(choice_save_image).is_dir() == True:
			image.save(choice_save_image + "/" + name__image)
			
		if os.path.exists(choice_save_image + "/" + name__image):
			return colored("Photo added successfully","green")
		return colored("Error Is Not Add Image!") 
			
		
	except Exception as error:
		return error 	

		
#if __name__=="__main__":
#	path_images = input("Enter Your Path Images :")
#	check_path = PathValidation(path_images)
#	
#	result1 = CheckThePath(check_path)
#	
#	
#	
#	result2 = SortingImagesFromDatat(result1)
#	result3 = AddPhotosFolder(result2)
#	
#	result4 = CopyPhotosAnotherFolder(result3 , result2 , result1)
#	print(ShowLargerImage(result3))

#		
